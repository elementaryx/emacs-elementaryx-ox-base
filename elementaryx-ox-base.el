(use-package elementaryx-org-minimal)
(use-package ob-latexmacro)
(use-package ob-latexpicture)

(use-package org
  :defer t
  :config
  ;; Use org-babel-lob-ingest-from-emacslobpath to load lob-ob-latexpicture.org
  (org-babel-lob-ingest-from-emacslobpath "lob-ob-latexpicture.org")
  ;; Allow to emphasize letters in the middle of a word
  (setcar org-emphasis-regexp-components " \t('\"{[:alpha:]")
  (setcar (nthcdr 1 org-emphasis-regexp-components) "[:alpha:]- \t.,:!?;'\")}\\")
  (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components))

(use-package org
  :defer t
  :custom
  ;; Declare some remote resources as safe so that elementaryx can
  ;; fetch them (through #+SETUPFILE: or #+INCLUDE org-mode
  ;; directives)
  (org-safe-remote-resources
   '("\\`https://gitlab\\.inria\\.fr/elementaryx/.*"
     "\\`https://elementaryx\\.gitlabpages\\.inria\\.fr/.*"
     "\\`https://gitlab\\.inria\\.fr/compose/.*"
     "\\`https://compose\\.gitlabpages\\.inria\\.fr/.*"
     "\\`https://gitlab\\.inria\\.fr/concace/.*"
     "\\`https://concace\\.gitlabpages\\.inria\\.fr/.*")
   ))

(use-package ox-extra ;; guix emacs-org-contrib package
  :defer t
  :config
  ;; Ignore headlines with :ignore: tag:
  (ox-extras-activate '(ignore-headlines)))

(use-package ox
  :defer t
  :config
  ;; Define a function for selective content export based on the backend.
  (defun elementaryx/filter-export (backend)
    (if (org-export-derived-backend-p backend 'latex)
	(org-map-entries '(org-toggle-tag "noexport" "on") "+htmlonly"))
    (if (org-export-derived-backend-p backend 'html)
	(org-map-entries '(org-toggle-tag "noexport" "on") "+texonly")))
  (add-hook 'org-export-before-parsing-functions 'elementaryx/filter-export))

;; org-cite (oc.el): we choose the biblatex export processor for latex (and
;; derived back-ends) documents and csl otherwise (in particular for html
;; export)
(use-package oc
  :defer t
  :custom
  (org-cite-export-processors
   '((latex biblatex)
     (t csl))))

(provide 'elementaryx-ox-base)
